{Meteor} = require 'meteor/meteor'
{check}  = require 'meteor/check'
Request  = require 'request'

TARGET_URL = 'https://mobileserver.vapteke.ru/mobile/getjson.php'
DEVICE_UID = Meteor.uuid()
DEVICE_AID = 'Android'

requestBase = Request.defaults
	gzip    : true
	headers :
		'User-Agent' : 'ru.vapteke.app:5',
		'Charset'    : 'utf-8',
		'Host'       : 'mobileserver.vapteke.ru',
		'Connection' : 'Keep-Alive'


makeRequestAsync = Meteor.wrapAsync (qs, callback) ->
	url = TARGET_URL

	qs.device = DEVICE_AID
	qs.id     = DEVICE_UID

	requestBase {url, qs}, (err, res, body) ->
		try
			callback(null, JSON.parse(body))
		catch e
			callback(new Error('Cant parse response'))


Meteor.methods
	getTips: (tip) ->
		check(tip, String)

		if tip.length < 3 then throw (new Meteor.Error(504))

		query =
			s : tip
			procedure: 'usp_NameLecSearch'

		data = makeRequestAsync(query)

		_.map (data?.DataRows or []), (item) -> item.Value

	getSearches: (search, city = 0) ->
		check(search, String)
		check(city, Number)

		if search.length < 3 then throw (new Meteor.Error(504))

		query =
			s         : search.toLowerCase()
			City_ID   : city or 2
			procedure : 'usp_TovarSearchWebTNFull_test'

		data = makeRequestAsync(query)

		products = {}
		_.each (data.DataRows or []), (item) ->
			products[item.KodTovar] =
				id    : item.KodTovar
				name  : item.TovarName
				price : Number(item.AVGPrice or '0')
		{products}

	getOffers: (id, cx = 0, cy = 0, city = 0) ->
		check(id, String)
		check(cx, Number)
		check(cy, Number)
		check(city, Number)

		query =
			KodTovar  : id
			City_ID   : city or 2
			CoordX    : cx or 82.9348245
			CoordY    : cy or 55.0688816
			KodApt    : 0
			procedure : 'usp_TovarSearchWithPriceSpWeb_test'

		data = makeRequestAsync(query)

		offers = {}
		_.each (data.DataRows or []), (item) ->
			offers[item.KodApt] =
				id    : item.KodApt
				name  : item.AptName
				cx    : item.CoordX
				cy    : item.CoordY
				h24   : (item.Hours24 is 1)
				price : Number(item.Price or '0')
				loc   : item.Adres
		{offers}
