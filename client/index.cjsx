React                           = require('react')
ReactDOM                        = require('react-dom')
injectTapEventPlugin            = require('react-tap-event-plugin')
{Router, Route, IndexRoute}     = require('react-router')

{History} = require('./views/history.cjsx')


{Layout} = require('./views/layout.cjsx')
{Index}  = require('./views/index.cjsx')
{Search} = require('./views/search.cjsx')
{Offers} = require('./views/offers.cjsx')

routes = ->
	(<Router history={History}>
		<Route path='/search' component={Search}></Route>
		<Route path='/' component={Layout}>
			<Route path='/offers' component={Offers}></Route>
			<IndexRoute component={Index}/>
		</Route>
	</Router>)

Meteor.startup ->
	console.log 'startup'
	injectTapEventPlugin()

	appDiv = document.getElementById('app')
	ReactDOM.render(routes(), appDiv)


# <Route path='/suggests' component={null}></Route>
# <Route path='/scan' component={null}></Route>
# <Route path='/search' component={null}></Route>
# <Route path='/offers/:view' component={null}></Route>
