React      = require('react')
GoogleMap  = require('google-map-react')

MUI        = require('material-ui')
MUIStyle   = require('material-ui/lib/styles')
MUIIcons   = require('material-ui/lib/svg-icons')

{MapStyle} = require('./mapstyle.cjsx')

{Products} = require('./productStore.cjsx')

{History}  = require('./history.cjsx')

PRIMARY_BGCOLOR = '#fcfcfc'
MARKER_SIZE     = 30
MAP_API_KEY     = 'AIzaSyBOZboNzX5LnvpitO3dhpGEkxRnDOa6J60'



exports.Offers = React.createClass
	displayName: 'Offers'

	mixins: [MUI.Mixins.StyleResizable]

	getInitialState: ->
		_.extend Products.mapAll(), {trader: null, item: null}

	componentDidMount: ->
		Products.update (state) => @setState(state)

	componentWillUnmount: ->
		Products.stopUpdater()

	onToggle: (id) ->
		Products.toggle id, (state) => @setState(state)
	onDisableOther: (id) ->
		Products.disableOmit id, (state) => @setState(state)
	onRemove: (id) ->
		Products.remove id, (state) => @setState(state)
	onRemoveOher: (id) ->
		Products.removeOmit id, (state) => @setState(state)

	onToggleParam: (key) ->
		Products.toggleParam key, (state) => @setState(state)

	onSearch: (search) ->
		if search and search.length > 2
			History.push("/search?search=#{search}")

	render: ->
		{products, offers, params} = @state

		propsProducts = {products, @onToggle, @onDisableOther, @onRemove, @onRemoveOher, @onSearch}
		propsOffers   = {offers}
		propsParams   = {params, @onToggleParam}

		ctrStyle =
			width       : '100%'
			height      : '100%'

		ictStyle =
			width       : '100%'
			height      : '100%'
			overflowY   : 'auto'

		fcStyle =
			position    : 'absolute'
			left        : 0
			top         : 0
			width       : 380
			height      : '100%'
			borderRight : '1px solid #e0e0e0'
			zIndex      : 1

		scStyle =
			position    : 'absolute'
			left        : 380
			top         : 0
			width       : 460
			height      : '100%'
			borderRight : '1px solid #e0e0e0'
			zIndex      : 1

		mcStyle =
			width       : '100%'
			height      : '100%'
			paddingLeft : 380 + 460

		{Divider} = MUI

		(<div style={ctrStyle}>
			<div style={fcStyle}>
				<div style={ictStyle}>
					<OfferParams {...propsParams}/>
					<Divider/>
					<OffersProducts {...propsProducts}/>
				</div>
			</div>
			<div style={scStyle}>
				<div style={ictStyle}>
					<OfferOffers {...propsOffers}/>
				</div>
			</div>
			<div style={mcStyle}>
				<OffersMap offers={offers}/>
			</div>
		</div>)


OffersProducts = ({products, onToggle, onDisableOther, onRemove, onRemoveOher, onSearch}) ->
	{List, ListItem, IconButton, Divider, Checkbox, IconMenu, MenuItem, Divider} = MUI
	{NavigationMoreVert} = MUIIcons

	innerDivStyle =
		paddingLeft: 60
	innerHrStyle =
		marginLeft: 60
	subTextStyle =
		marginTop: 6

	iconButtonItemMenu = (<IconButton touch={true}><NavigationMoreVert/></IconButton>)

	makeItemMenu = ({id, search}) ->
		doRemove       = -> onRemove(id)
		doRemoveOther  = -> onRemoveOher(id)
		doSearch       = -> onSearch(search)
		doDisableOther = -> onDisableOther(id)

		(<IconMenu iconButtonElement={iconButtonItemMenu}>
			<MenuItem onTouchTap={doSearch}>Найти подобные</MenuItem>
			<Divider/>
			<MenuItem onTouchTap={doDisableOther}>Выключить другие</MenuItem>
			<Divider/>
			<MenuItem onTouchTap={doRemove}>Удалит из списка</MenuItem>
			<MenuItem onTouchTap={doRemoveOther}>Удалить все другие</MenuItem>
		</IconMenu>)

	makeItemCheckbox = ({id, active}) ->
		doToggle = -> onToggle(id)

		(<Checkbox checked={active} onCheck={doToggle}></Checkbox>)

	makeItemSubtext = ({min, max, count}) ->

		min = min?.toFixed?(2)
		max = max?.toFixed?(2)

		pricesText = if (min and max) then (<span>от <b>{min}</b> до <b>{max}</b> руб.</span>) else null
		awailText  = if count then (<span> аптек <span>{count}</span> </span>) else null

		(<div style={subTextStyle}>{pricesText}{awailText}</div>)

	list = _.map products, (item) ->
		leftCheckbox = makeItemCheckbox(item)
		rightMenu    = makeItemMenu(item)
		subText      = makeItemSubtext(item)

		itemProps =
			primaryText        : item.name
			secondaryText      : subText
			secondaryTextLines : 1
			leftCheckbox       : leftCheckbox
			rightIconButton    : rightMenu
			innerDivStyle      : innerDivStyle

		(<div key={item.id}>
			<ListItem {...itemProps}/>
			<Divider style={innerHrStyle}/>
		</div>)

	props =
		subheader : "Список товаров (#{products.length})"
		style     : {backgroundColor: PRIMARY_BGCOLOR}

	(<List {...props}>{list}</List>)


OfferParams = ({params, onToggleParam}) ->
	{List, ListItem, Toggle, Divider} = MUI

	props =
		toggled  : params.awailAll
		onToggle : -> onToggleParam('awailAll')
	toggle = (<Toggle {...props}/>)

	props =
		primaryText        : 'Все товары в одном месте'
		secondaryText      : 'Отображать только аптеки с наличием всех товаров из списка'
		secondaryTextLines : 2
		rightToggle        : toggle

	itemAwailAll = (<ListItem {...props}/>)

	props =
		toggled  : params.hours24
		onToggle : -> onToggleParam('hours24')
	toggle = (<Toggle {...props}/>)

	props =
		primaryText        : 'Круглосуточно'
		secondaryText      : 'Отображать только аптеки работающие 24 часа'
		secondaryTextLines : 2
		rightToggle        : toggle

	itemHours24 = (<ListItem {...props}/>)

	props =
		subheader : "Параметры списка"
		style     : {backgroundColor: PRIMARY_BGCOLOR}

	(<List {...props}>
		{itemAwailAll}
		{itemHours24}
	</List>)

calcColor = (index, len) ->
	from = {r: 56, g: 142, b: 60}
	to   = {r: 211, g: 47, b: 47}

	r = Math.floor (to.r - from.r) / len * index + from.r
	g = Math.floor (to.g - from.g) / len * index + from.g
	b = Math.floor (to.b - from.b) / len * index + from.b

	{r, g, b}

OfferOffers = ({offers}) ->
	{List, ListItem, Toggle, Divider} = MUI

	expStyle =
		# fontWeight: 'bold'
		color: MUIStyle.Colors.red500

	chpStyle =
		fontWeight: 'bold'
		color: MUIStyle.Colors.green500

	makeProductsList = (list) ->
		_.map list, ({id, name, price, delta, min}) ->
			deltaText = if delta is 0 then '' else " + #{delta.toFixed(2)} дороже"

			subText = (<p>
				<span>цена </span><b>{"#{price.toFixed(2)} "}</b>
				<span>мин </span><b>{"#{min.toFixed(2)} "}</b>
				<span style={expStyle}>{deltaText}</span>
			</p>)

			props =
				primaryText        : name
				secondaryText      : subText
				secondaryTextLines : 1

			(<ListItem key={id} {...props}/>)

	makeOffersList = (items) ->
		_.map items, ({id, name, loc, price, delta, list}, index) ->
			color = calcColor(index, offers.length)
			deltaStyle = {color: "rgb(#{color.r},#{color.g},#{color.b})"}
			deltaText  = if delta is 0 then 'самое выгодное' else " + #{delta.toFixed(2)} дороже"

			subText = (<span>
				<p>{loc}</p>
				<p><span>всего </span><b>{"#{price.toFixed(2)} "}</b><span style={deltaStyle}>{deltaText}</span></p>
			</span>)

			props =
				primaryText              : name
				secondaryText            : subText
				secondaryTextLines       : 1
				primaryTogglesNestedList : true
				nestedItems              : makeProductsList(list)

			(<div key={id}><ListItem {...props}/><Divider/></div>)

	props =
		subheader : "Список предложений/аптек (#{offers.length})"
	(<List {...props}>{makeOffersList(offers)}</List>)


OffersMap = React.createClass

	getInitialState: ->
		{zoom: 12, center: [55.0688816, 82.9348245], opened: null}

	onMapParams: ({zoom, center}) ->
		@setState {zoom, center}

	render: ->
		{zoom, center, opened} = @state
		{offers, trader}       = @props

		props = {@onMapParams, zoom, center, opened, offers, trader}

		(<MapBoundary {...props}/>)

MapBoundary = ({zoom, center, offers, trader, onMapParams}) ->
	{CommunicationLocationOn} = MUIIcons


	mapMarkers = (list) ->

		getMarkerStyle = (id, index) ->
			size = if id is trader then MARKER_SIZE * 1.4 else MARKER_SIZE

			{r, g, b} = calcColor(index, offers.length)
			style =
				position : 'absolute'
				width    : size
				height   : size
				top      : size * (-0.5)
				left     : size * (-0.5)
				fill     : "rgba(#{r},#{g},#{b},1)"


		getIconBox = (id) ->
			size = if id is trader then MARKER_SIZE * 1.4 else MARKER_SIZE
			size = 24 - size / 24
			"0 0 #{size} #{size}"

		_.map list, ({id, cx, cy}, index) ->
			props =
				key     : id
				style   : getMarkerStyle(id, index)
				viewBox : getIconBox(id)
				lat     : cy
				lng     : cx

			(<CommunicationLocationOn {...props}/>)

	setMapOptions =  ->
		{styles: MapStyle}

	props =
		bootstrapURLKeys : {key: MAP_API_KEY, language: 'ru'}
		center           : center
		zoom             : zoom
		onChange         : onMapParams
		options          : setMapOptions


	list = mapMarkers(offers)
	<GoogleMap {...props}>{list}</GoogleMap>
