React     = require('react')
GoogleMap = require('google-map-react')

MUI       = require('material-ui')
MUIStyle  = require('material-ui/lib/styles')
MUIIcons  = require('material-ui/lib/svg-icons')

MARKER_SIZE = 24
API_KEY     = 'AIzaSyBOZboNzX5LnvpitO3dhpGEkxRnDOa6J60'

exports.OffersMap = ({offers}) ->
	{CommunicationLocationOn} = MUIIcons

	markerStyle =
		position : 'absolute'
		width    : MARKER_SIZE
		height   : MARKER_SIZE
		top      : MARKER_SIZE * (-0.5)
		left     : MARKER_SIZE * (-0.5)

	mapMarkers = (list) ->
		_.map list, ({id, cx, cy}) ->
			<CommunicationLocationOn key={id} style={markerStyle} lat={cx} lng={cy}/>

	list = mapMarkers(offers)
	<GoogleMap apiKey={API_KEY}>{list}</GoogleMap>
