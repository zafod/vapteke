React                     = require('react')

MUI                       = require('material-ui')
MUIStyle                  = require('material-ui/lib/styles')
MUIIcons                  = require('material-ui/lib/svg-icons')

{History} = require('./history.cjsx')

exports.Layout = React.createClass
	displayName: 'Layout'

	getInitialState: ->
		{menuOpened: false}

	onOpen: ->
		@setState {menuOpened: true}

	onClose: ->
		@setState {menuOpened: false}

	render: ->
		wrapProps =
			children : @props.children
			style    : {width: '100%', height: '100%', overflow: 'hidden', paddingTop: 64}

		barProps = {@onOpen}
		menuProps = {@onClose}

		{AppCanvas} = MUI

		(<AppCanvas>
			<LayoutBar {...barProps}/>
			<div {...wrapProps}/>
		</AppCanvas>)

LayoutBar = ({onOpen}) ->
	{AppBar, IconButton} = MUI
	{ActionSearch}       = MUIIcons

	onSearch = ->
		History.push('/search')

	iconRight  = (<ActionSearch/>)

	iconRightProps =
		onTouchTap : onSearch
		children   : iconRight

	buttonRight = (<IconButton {...iconRightProps}/>)

	barProps =
		iconElementRight         : buttonRight
		onLeftIconButtonTouchTap : onOpen
		title                    : 'Fapteke'
		style                    : {position: 'fixed', top: 0}

	(<AppBar {...barProps}/>)
