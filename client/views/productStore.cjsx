
exports.Products = class @Products
	@traders : {}
	@values  : []
	@params  :
		sortFor  : 0
		hours24  : false
		awailAll : true

	@interval    : null
	@updateCycle : 1

	@toggleParam: (key, callback) ->
		if _.has @params, key
			@params[key] = not @params[key]
		@mapAllAndCallback(callback)

	@add: (item, callback) ->
		id = item?.id
		unless @list[id]
			cycle = @updateCycle - 1
			item  = _.chain(item)
				.pick('id', 'search', 'name', 'price')
				.extend({cycle, offers: {}, active: true})
				.value()

			@list[id] = item

		@mapAllAndCallback(callback)

	@remove: (id, callback) ->
		@list = _.omit(@list, id)

		@mapAllAndCallback(callback)

	@removeOmit: (id, callback) ->
		if item = @list[id]
			@list = {}
			@list[id] = item

		@enable(id, callback)

	@disable: (id, callback) ->
		@list[id]?.active = false

		@mapAllAndCallback(callback)

	@disableOmit: (id, callback) ->
		for key of @list
			@disable(key)

		@enable(id, callback)

	@enable: (id, callback) ->
		@list[id]?.active = true

		@mapAllAndCallback(callback)

	@toggle: (id, callback) ->
		if item = @list[id]
			@list[id].active = not item.active

		@mapAllAndCallback(callback)

	@afterUpdateAll: (callback) ->
		cycle    = @updateCycle
		@values  = _.where(@values, {cycle})
		@traders = _.chain(@traders).where({cycle}).indexBy('id').value()

		# map products now
		for id, props of @list
			unless props.active then continue
			prices = _.chain(@values)
				.where({item: id})
				.pluck('price')
				.value()
			props.count = _.size(prices)
			props.min   = _.min prices
			props.max   = _.max prices

			@list[id] = props
		callback()

	@afterUpadate: (id, offers, callback) ->
		if @list[id]
			@list[id].cycle = @updateCycle

			for key, offer of (offers or {})
				trader        = offer
				trader.cycle  = @updateCycle
				@traders[key] = trader

				value =
					cycle  : @updateCycle
					trader : key
					item   : id
					price  : offer.price
				@values.push(value)

		@update(callback)


	@update: (callback) ->
		item = _.find @list, (item) =>
			item.active and (item.cycle or 0) < @updateCycle

		unless (id = item?.id)
			@afterUpdateAll =>
				@runUpdater(callback)
				@mapAllAndCallback(callback)
		else
			@stopUpdater()
			Meteor.call 'getOffers', id, (err, res) =>
				@afterUpadate(id, res?.offers, callback)

	@runUpdater: (callback) ->
		updateHandle = () =>
			@updateCycle++
			@update(callback)

		@stopUpdater()
		@interval = Meteor.setInterval updateHandle, 180000

	@stopUpdater: ->
		if @interval then @interval = Meteor.clearInterval(@interval)

	@mapAll: ->
		{@params, products: @mapList(), offers: @mapOffers()}

	@mapAllAndCallback: (callback) ->
		if callback then callback @mapAll()

	@mapList: ->
		_.chain(@list)
			.map ({id, search, min, max, price, count, name, active}) ->
				{id, search, min, max, price, count, name, active}
			.sortBy('search')
			.value()

	@mapOffers: ->
		{sortFor, hours24, awailAll} = @params

		items = _.chain(@list)
			.filter ({active}) -> active
			.pluck 'id'
			.value()

		itemsLen = _.size(items)
		noCheckItems = _.size(@items) is itemsLen

		traders = _.chain(@traders)
			.filter ({h24}) -> not(hours24) or h24
			.pluck 'trader'
			.value()

		noCheckTraders = _.size(@traders) is _.size(traders)


		_.chain(@values)
			.filter ({item, trader}) ->
				(noCheckItems or (item in items)) and (noCheckTraders or (trader in traders))
			.map ({item, trader, price}) =>
				data = @list[item]
				_.extend {}, data, {item, trader, price}, {delta: price - data.min}
			.groupBy('trader')
			.map (list, trader) =>
				reducer = (memo, {price, delta}) ->
					memo.price = (memo.price or 0) + price
					memo.delta = (memo.delta or 0) + delta
					memo.count = (memo.count or 0) + 1
					return memo

				memo = _.reduce list, reducer, {}

				list = _.map list, ({id, search, price, delta, name, min}) ->
					{id, search, price, delta, name, min}

				_.extend {}, @traders[trader], memo, {list}
			.filter ({count}) ->
				not(awailAll) or (count is itemsLen)
			.sortBy ({count, price}) ->
				price
			.value()
