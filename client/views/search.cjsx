React      = require('react')

MUI        = require('material-ui')
MUIStyle   = require('material-ui/lib/styles')
MUIIcons   = require('material-ui/lib/svg-icons')

{Searches} = require('./searchStore.cjsx')
{Products} = require('./productStore.cjsx')

{History}  = require('./history.cjsx')


exports.Search = React.createClass
	displayName: 'Search'

	getInitialState: ->
		Searches.mapAll()

	componentDidMount: ->
		@input = document.querySelector('#search')
		@input.setAttribute('autocomplete', 'off')

		@onSearch(@props.location?.query?.search)

	onTip: (tip = '') ->
		Searches.suggest tip, (state) => @setState(state)

	onSearch: (search) ->
		@input.value = search or ''
		Meteor.defer => @input.focus()

		Searches.find search, (state) => @setState(state)

	onProduct: (product) ->
		if _.isObject(product)
			product.search = @state.search
			Products.add(product)
			History.push('/offers')

	render: ->
		{loading, tips, search, products} = @state

		props       = {@onTip, @onSearch, search}
		compField   = (<SearchField {...props}/>)

		props       = {@onSearch, field: compField}
		compBar     = (<SearchBar {...props}/>)

		if loading
			props       = {loading}
			compContent = (<SearchLoading {...props}/>)
		else if search
			props       = {@onProduct, products, search}
			compContent = (<SearchProducts {...props}/>)
		else
			props       = {@onSearch, tips, search}
			compContent = (<SearchTips {...props}/>)

		props       = {style: {paddingTop: 64}, children: compContent}
		compWrap    = (<div {...props}/>)

		{AppCanvas} = MUI

		(<AppCanvas>{compBar}{compWrap}</AppCanvas>)

SearchBar = ({field, onSearch}) ->
	{AppBar, IconButton}                = MUI
	{ContentClear, NavigationArrowBack} = MUIIcons

	onClear = -> onSearch(null)
	goBack  = -> History.goBack()

	iconsColor = MUIStyle.Colors.grey700

	inconLeft   = (<NavigationArrowBack color={iconsColor}/>)
	buttonLeft  = (<IconButton onTouchTap={goBack}>{inconLeft}</IconButton>)

	inconRight  = (<ContentClear color={iconsColor}/>)
	buttonRight = (<IconButton onTouchTap={onClear}>{inconRight}</IconButton>)

	barProps =
		iconElementLeft  : buttonLeft
		iconElementRight : buttonRight
		title            : field
		style            : {position: 'fixed', backgroundColor: 'white'}

	(<AppBar {...barProps}/>)

SearchField = ({search, onTip, onSearch}) ->
	{TextField} = MUI

	props =
		hintText       : if search then '' else 'Поиск'
		id             : 'search'
		fullWidth      : true
		underlineShow  : false
		style          : {marginRight: 16}
		onChange       : (e) -> onTip(e.target?.value)
		onEnterKeyDown : (e) -> onSearch(e.target?.value)

	(<TextField {...props}/>)

SearchTips = ({search, tips, onSearch}) ->
	{List, ListItem} = MUI

	items = _.map tips, (text, index) ->
		props =
			primaryText : text
			key         : index
			onTouchTap  : -> onSearch(text)
		(<ListItem {...props}/>)

	(<List>{items}</List>)

SearchProducts = ({search, products, onProduct}) ->
	{List, ListItem, FlatButton} = MUI

	listHeader = "Результаты для \"#{search}\""

	items = _.map products, (item) ->
		icon     = MUIIcons['FileFolder']
		color    = MUIStyle.Colors['pink400']
		leftIcon = (<icon color={color}/>)

		props =
			label     : item.price.toFixed(2)
			secondary : true
			style     : {textAlign: 'right', lineHeight: '24px', pointerEvents: 'none'}

		rightButton = (<FlatButton {...props}/>)

		props =
			leftIcon      : leftIcon
			rightIcon     : rightButton
			innerDivStyle : {paddingRight: 108, paddingLeft: 64}
			primaryText   : item.name
			secondaryText : item.name
			key           : item.id
			onTouchTap    : -> onProduct(item)
		(<ListItem {...props}/>)

	(<List subheader={listHeader}>{items}</List>)

SearchLoading = ({loading}) ->
	wrapStyle =
		marginTop: 40
		marginLeft: 'auto'
		marginRight: 'auto'
		width: 48
		height: 48

	{RefreshIndicator} = MUI

	(<div style={wrapStyle}><RefreshIndicator status='loading' top={0} left={0}/></div>)
