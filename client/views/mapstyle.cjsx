

exports.MapStyle = [ {
  'featureType': 'all'
  'elementType': 'all'
  'stylers': [
    { 'saturation': '-65' }
    { 'gamma': '0.54' }
    { 'lightness': '6' }
  ]
} ]
