
exports.Searches = class @Searches
	@search   : ''
	@products : {}
	@loading  : false

	@tip      : ''
	@tips     : []
	@running  : false

	@suggest: (str, callback) ->
		unless str then return
		@search   = ''
		@products = ''

		str  = (str or '').trim()
		tip  = @tip
		@tip = str

		if not(str) or str.length < 3
			@mapAllAndCallback(callback)
		else
			exp = new RegExp "^#{tip}$", "gi"
			if not(str.match(exp)) or @tips.length > 10
				Meteor.call 'getTips', str, (err, res) =>
					@tips = res or []
					@mapAllAndCallback(callback)
			else
				@mapAllAndCallback(callback)

	@find: (search, callback) ->
		@products = {}
		@search   = search

		unless search
			@mapAllAndCallback(callback)
		else
			@loading = true

			callback {@loading}
			Meteor.call 'getSearches', search, (err, res) =>
				@loading  = false
				if products = res?.products
					for id, item of products
						item.search = search
						@products[id] = item

				@mapAllAndCallback(callback)

	@mapTips: ->
		exp = new RegExp "^#{@tip}", "i"
		_.filter @tips, (str) -> str.match(exp)

	@mapProducts: ->
		_.chain(@products)
			.sortBy('id')
			.value()

	@mapAll: ->
		{@search, @loading, tips: @mapTips(), products: @mapProducts()}

	@mapAllAndCallback: (callback) ->
		if(callback) then callback @mapAll()
